<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8">
    <!-- Nous chargeons les fichiers CDN de Leaflet. Le CSS AVANT le JS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/leaflet.markercluster@1.5.3/dist/MarkerCluster.css" />
    <link rel="stylesheet" type="text/css"
        href="https://unpkg.com/leaflet.markercluster@1.5.3/dist/MarkerCluster.Default.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="index.css" />
    <title>Les Petits Ateliers de Marcel</title>
</head>

<body>
    <div class="main">
        <h1>Les Petits Ateliers de Marcel</h1>
        <div id="map">
            <!-- Ici s'affichera la carte -->
        </div>

        <img src="marker-icon.png" style="height: 30px"> : Membres
        <img src="marker-icon-red.png" style="height: 30px"> : Lieu mis à disposition pour des ateliers (sous
        conditions)

        <h3 style="color: green; display: none;" id="success">Le membre a bien ete ajoute</h3>
        <h3 style="color: red; display: none;" id="error">Petit problème lors de l'enregistrement.</h3>

        <h2>Ajouter un membre</h2>
        <form method="POST" id="create_form" style="margin-bottom:5px" class="form-inline">
            <div>
                <div class="form-group">
                    Pseudo Facebook (*) : <input style="width: 100%" type="text" name="name" id="name" required
                        pattern="^.{3,}$">
                </div>
                <div class="form-group">
                    Adresse : <input type="text" name="adress" id="adress">
                </div>
                <div class="form-group">
                    Code postal (*) : <input type="text" name="postalcode" id="postalcode" required>
                </div>
                <div class="form-group">
                    Ville (*) : <input type="text" name="city" id="city" required>
                </div>
                <div class="form-group">
                    Pays (*) : <input type="text" name="country" id="country" value="France" required>
                </div>
                <div id=result_search></div>
                <div class="form-group">
                    Email : <input type="email" name="email" id="email">
                </div>
                <div class="form-group">
                    Telephone : <input type="phone" name="phone" id="phone">
                </div>
                <div class="form-group">
                    Seriez-vous pret à accueillir des ateliers dans votre lieu (*) ?
                    <span><label style="display: inline" for="reception">Oui</label><input
                            style="display: inline; width: unset;" type="radio" name="reception" value="1"
                            required></span>
                    <span><label style="display: inline" for="reception">Non</label><input
                            style="display: inline; width: unset;" type="radio" name="reception" value="0"
                            required></span>

                </div>
                <div class="form-group">
                    <div id="descriptif">
                        Descriptif sommaire de ce qui est proposé : <textarea style="width: 100%" name="description"
                            id="description"></textarea>
                    </div>
                </div>
                <p>
                    <span class="bold">(*) : Champs obligatoires</span>
                </p>
                <button type="submit" class="btn btn-primary" id="submitCreate">Previsulaiser</button>
            </div>

        </form>

        <div id="preview" style="display: none">
            <div>
                <span class="bold">Pseudo Facebook : </span><span id="preview_pseudo"></span>
            </div>
            <div>
                <span class="bold">Localisation : </span><span id="preview_localisation"></span>
            </div>
            <div>
                <span class="bold">Email : </span><span id="preview_email"></span>
            </div>
            <div>
                <span class="bold">Telephone : </span><span id="preview_phone"></span>
            </div>
            <div id="preview_descriptif">
                <span class="bold">Descriptif de votre atelier : </span><span id="preview_description"></span>
            </div>
            <span id="preview_lat" style="display:none"></span>
            <span id="preview_lon" style="display:none"></span>
            <span id="preview_reception" style="display:none"></span>
            <button onclick=submitForm()>Confirmer</button>
            <button onclick=cancelSubmit()>Modifier</button>
        </div>
    </div>


    <!-- Fichiers Javascript -->
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    <script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.5.3/dist/leaflet.markercluster.js'>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
        </script>

    <script type="text/javascript" src="index.js"></script>

</body>

</html>