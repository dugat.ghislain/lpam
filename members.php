<?php

require_once('config.php');

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

// Create connection
$conn = new mysqli(
    $servername,
    $username,
    $password,
    $dbname
);

// Check connection
if ($conn->connect_error) {
    die("Problème avec la BDD.");
}

$result = $conn->query('SELECT * FROM members');
echo json_encode($result->fetch_all(MYSQLI_ASSOC));
?>