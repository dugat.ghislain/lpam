<?php

require_once('config.php');

if ($_POST['pseudo'] ?? null && preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $_POST['lat']) && preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $_POST['lon'])) {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    // Create connection
    $conn = new mysqli(
        $servername,
        $username,
        $password,
        $dbname
    );

    // Check connection
    if ($conn->connect_error) {
        die("Problème avec la BDD.");
    }

    $stmt = $conn->prepare("INSERT INTO members VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssissss", $_POST['pseudo'], $_POST['lat'], $_POST['lon'], $_POST['reception'], $_POST['localisation'], $_POST['email'], $_POST['phone'], $_POST['description']);

    if ($stmt->execute()) {
        die("ok");
    }

}
echo "Fromulaire pas bon.";
?>