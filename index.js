var myMap = null;
var markerClusters = L.markerClusterGroup();
var markerSearchCluster;
markerSearchCluster = L.markerClusterGroup();
var markerSearch;
var markerPreviewCluster;
markerPreviewCluster = L.markerClusterGroup();
var markerPreview;

$(document).ready(function () {
    initMap();

    $('input[type=radio][name=reception]').change(function () {
        if (this.value == '1') {
            $("#descriptif").show()
        } else {
            $("#descriptif").hide()
        }
    });
    $("#create_form").submit(function (event) {
        event.preventDefault();
        var formData = {
            format: "json",
            street: $("#adress").val(),
            postalcode: $("#postalcode").val(),
            city: $("#city").val(),
            country: $("#country").val(),
        };

        var url = "https://nominatim.openstreetmap.org/search"

        $.ajax({
            type: "GET",
            url: url,
            data: formData,
            dataType: "json",
            encode: true,
        }).done(function (data) {
            var localisationInData = data.find((o) => {
                return o["display_name"] === $('#preview_localisation').text()
            })
            if (data.length == 0) {
                $('#result_search').html(
                    "<h3 style='color:red'>L'adresse n'est pas bonne.</h3>");
            } else if (data.length > 1 &&
                ($('#preview_localisation').text().length == 0 || localisationInData ==
                    undefined)) {
                var content =
                    "<h3 style='color:red'>Il y a plusieurs localisations possibles, merci d'en choisir une.</h3><table style='margin-bottom: 10px'>"
                for (i = 0; i < data.length; i++) {
                    content += '<tr><td>' + data[i].display_name +
                        '</td><td><button type="button" onclick=clickLocation(this) class="choose_latlong btn btn-light" data-localisation="' +
                        data[i].display_name + '" data-lat="' +
                        data[i].lat + '" data-lon="' + data[i].lon +
                        '">Choisir</button></td></tr>';
                }
                content += "</table>"

                $('#result_search').html(content);
            } else {
                $('#result_search').html("");
                if (data.length == 1) {
                    $('#preview_localisation').html(data[0].display_name)
                    $('#preview_lat').html(data[0].lat)
                    $('#preview_lon').html(data[0].lon)
                }
                markerSearchCluster.clearLayers()
                markerPreviewCluster.clearLayers()
                $('#preview_pseudo').html($("#name").val())
                $('#preview_email').html($("#email").val())
                $('#preview_phone').html($("#phone").val())
                $('#preview_description').html($("#description").val())
                $('#preview_reception').html($('input[type=radio][name=reception]:checked').val())
                var iconUrl;
                if ($('input[type=radio][name=reception]:checked').val() == "1") {
                    $('#preview_descriptif').show()
                    iconUrl = 'marker-icon-red.png'
                } else {
                    $('#preview_descriptif').hide()
                    iconUrl = 'marker-icon.png'
                }

                var myIcon = L.icon({
                    iconUrl: iconUrl,
                });
                markerPreview = L.marker([$('#preview_lat').text(), $('#preview_lon')
                    .text()
                ], {
                    icon: myIcon
                });
                markerPreviewCluster.addLayer(markerPreview);
                myMap.setView(new L.LatLng($('#preview_lat').text(), $('#preview_lon')
                    .text()), 11);


                $('#create_form').hide()
                $('#preview').show()
            }
        });
    });
});

function submitForm() {
    $.ajax({
        url: "/LPAM/createMember.php ",
        type: "POST",
        data: {
            pseudo: $('#preview_pseudo').text(),
            localisation: $('#preview_localisation').text(),
            email: $('#preview_email').text(),
            phone: $('#preview_phone').text(),
            description: $('#preview_description').text(),
            lat: $('#preview_lat').text(),
            lon: $('#preview_lon').text(),
            reception: $('#preview_reception').text(),
        },
        success: function (response) {
            if (response == "ok") {
                $('#preview_localisation').html("")
                $('#preview_lat').html("")
                $('#preview_lon').html("")
                $('#preview_pseudo').html("")
                $('#preview_email').html("")
                $('#preview_phone').html("")
                $('#preview_description').html("")
                $('#preview_reception').html("")


                $('#name').val("")
                $('#adress').val("")
                $('#postalcode').val("")
                $('#city').val("")
                $('#country').val("France")
                $('#email').val("")
                $('#phone').val("")
                $('#description').val("")
                $('input[name="reception"]').prop('checked', false)


                $('#preview').hide()
                $('#create_form').show()
                $('#success').show()
                setTimeout(
                    function () {
                        $('#success').hide()
                    },
                    5000);

                markerClusters.clearLayers();
                markerPreviewCluster.clearLayers();
                populateMap();

            } else {
                $('#error').show()
                setTimeout(
                    function () {
                        $('#error').hide()
                    },
                    5000);
            }
        },
        error: function () {
            $('#error').show()
            setTimeout(
                function () {
                    $('#error').hide()
                },
                5000);
        }

    });
}

function cancelSubmit() {
    $("#preview").hide()
    $('#create_form').show()
    if (markerPreview != null) {
        markerPreviewCluster.removeLayer(markerPreview);
    }
}

function clickLocation(e) {
    $(".choose_latlong").removeClass("btn-success")
    $(".choose_latlong").addClass("btn-light")
    $('#preview_localisation').html($(e).data("localisation"))
    $('#preview_lat').html($(e).data("lat"))
    $('#preview_lon').html($(e).data("lon"))
    markerSearchCluster.clearLayers()
    var myIcon = L.icon({
        iconUrl: 'marker-icon-green.png',
    });
    markerSearch = L.marker([$(e).data("lat"), $(e).data("lon")], {
        icon: myIcon
    });
    markerSearchCluster.addLayer(markerSearch);
    myMap.setView(new L.LatLng($(e).data("lat"), $(e).data("lon")), 11)

    $(e).removeClass("btn-light")
    $(e).addClass("btn-success")
}

function populateMap() {
    $.ajax({
        type: "GET",
        url: "/LPAM/members.php",
        dataType: "json",
        encode: true,
    }).done(function (data) {
        for (member in data) {
            var marker = null;
            if (data[member].reception == 1) {
                var myIcon = L.icon({
                    iconUrl: 'marker-icon-red.png',
                });
                marker = L.marker([data[member].lat, data[member].lon], {
                    icon: myIcon
                });

            } else {
                marker = L.marker([data[member].lat, data[member].lon]);
            }
            var text = "Pseudo Facebook : " + data[member].pseudo;
            if (data[member].email != '') {
                text += "<br>";
                text += "Email : " + data[member].email;
            }
            if (data[member].phone != '') {
                text += "<br>";
                text += "Telephone : " + data[member].phone;
            }
            if (data[member].reception == '1') {
                text += "<br>";
                text += "Description de l'atelier : " + data[member].description;
            }

            marker.bindPopup(text);
            markerClusters.addLayer(marker);
        }
    });
}


function initMap() {
    var zoom = 5
    var lat = 46.603354;
    var lon = 1.8883335;
    myMap = L.map('map').setView([lat, lon], zoom)
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(myMap);

    myMap.addLayer(markerClusters);
    myMap.addLayer(markerSearchCluster);
    myMap.addLayer(markerPreviewCluster);
    populateMap();
}